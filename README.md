# Code Challenge Mock Server

[![pipeline status](https://gitlab.com/publik/code-challenges/mock-server/badges/dev/pipeline.svg)](https://gitlab.com/publik/code-challenges/mock-server/commits/dev)

[Demo](http://code-challenge.k8s.blik-logistics.de/)

Code Owner: @pfxf

## Installation

### Run Python

Install pipenv:

```bash
pip install pipenv
```

Install dependencies:

```bash
pipenv install
```

Run Server with:

```bash
pipenv run python src/server.py
```

### Run Docker

We also provide a Docker image. To run the current head of the `dev` branch run:

```bash
sudo docker run -p 8000:8000 registry.gitlab.com/publik/code-challenges/mock-server:dev
```

To run the server at a given commit, the corresponding images is tagged with the commit hash.

## API Description

| Method | Call                                        | Description                                                               |
|:------:|:-------------------------------------------:|:-------------------------------------------------------------------------:|
| GET    | localhost:8000                              | for debugging purposes (not shown in frontend)                            |
| GET    | localhost:8000/analytics                    | provides a list of all available analytics resources                      |
| GET    | localhost:8000/analytics/\<ID>              | get information about a single analytics resource                         |
| GET    | localhost:8000/analytics/\<ID>/distribution | get distribution of load carriers, a load carrier is either empty or full |
| GET    | localhost:8000/analytics/\<ID>/delivery     | get deliveries per day (with ?week per week and with ?month per month)    |
| GET    | localhost:8000/analytics/\<ID>/throughput   | get process throughput per week                                           |

# Deployment

In order to deploy this server into your Kubernetes cluster run: 

```bash
kubectl create -f k8s/deployment.yaml
kubectl create -f k8s/service.yaml
kubectl create -f k8s/ingress.yaml
```