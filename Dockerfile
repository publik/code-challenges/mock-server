FROM python:alpine3.6

LABEL maintainer philipp.froehlich@blik.io

WORKDIR /root

COPY Pipfile .

COPY Pipfile.lock .

RUN pip install pipenv 

RUN pipenv install

COPY src ./src

WORKDIR /root/src

CMD ["pipenv", "run", "python", "-u", "server.py"]